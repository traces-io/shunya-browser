.. Copyright (c) 2020 Shunya Software

.. _rs_welcome:

Shunya browser documentation
---------------------------

This page has now moved to `shunya.com/linux <https://shunya.com/linux>`_
