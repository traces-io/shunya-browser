// Copyright (c) 2019 The Shunya Authors. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this file,
// you can obtain one at http://mozilla.org/MPL/2.0/.

const fs = require('fs')
const Log = require('../lib/logging')
const path = require('path')
const { spawnSync } = require('child_process')
const util = require('../lib/util')

Log.progress('Performing initial checkout of shunya-core')

const shunyaCoreDir = path.resolve(__dirname, '..', 'src', 'shunya')
const shunyaCoreRef = util.getProjectVersion('shunya-core')

if (!fs.existsSync(path.join(shunyaCoreDir, '.git'))) {
  Log.status(`Cloning shunya-core [${shunyaCoreRef}] into ${shunyaCoreDir}...`)
  fs.mkdirSync(shunyaCoreDir)
  util.runGit(shunyaCoreDir, ['clone', util.getNPMConfig(['projects', 'shunya-core', 'repository', 'url']), '.'])
  util.runGit(shunyaCoreDir, ['checkout', shunyaCoreRef])
}
const shunyaCoreSha = util.runGit(shunyaCoreDir, ['rev-parse', 'HEAD'])
Log.progress(`shunya-core repo at ${shunyaCoreDir} is at commit ID ${shunyaCoreSha}`)

let npmCommand = 'npm'
if (process.platform === 'win32') {
  npmCommand += '.cmd'
}

util.run(npmCommand, ['install'], { cwd: shunyaCoreDir })

util.run(npmCommand, ['run', 'sync' ,'--', '--init'].concat(process.argv.slice(2)), {
  cwd: shunyaCoreDir,
  env: process.env,
  stdio: 'inherit',
  shell: true,
  git_cwd: '.', })
